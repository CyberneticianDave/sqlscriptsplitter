<?php

$iMaxLines = 1000000;
if(isset($argv[2]) && is_numeric($argv[2])) {
    $iMaxLines = $argv[2];
} 

$sFilePath = $argv[1];
$aPathInfo = pathinfo($sFilePath);
$sFileName = $aPathInfo['basename'];
$sDirectory = $aPathInfo['dirname'];

// Read in a file
$oReadHandle = fopen($sFilePath, "r");
$oWriteHandle;

$i = 1;
$iLineCount = 0;
$sHeader = '';

if ($oReadHandle) {
    if(!is_dir($sDirectory . '\\' . $aPathInfo['filename'] . '\\')) {
        mkdir($sDirectory . '\\' . $aPathInfo['filename'] . '\\');
    }
    echo 'Starting to write ' . $sDirectory . '\\' . $aPathInfo['filename'] . '\\' . $aPathInfo['filename']  . '_' . $i . '.' . $aPathInfo['extension'] . PHP_EOL;
    $oWriteHandle = fopen($sDirectory . '\\' . $aPathInfo['filename'] . '\\' . $aPathInfo['filename'] . '_' . $i . '.' . $aPathInfo['extension'], 'w');

    while (($line = fgets($oReadHandle)) !== false) {
        // Get headers
        if(preg_match('/^(SET|\/\*!)/i', $line) != false) {
            $sHeader .= $line;
        }
        // line by line transfer to another file
        fwrite($oWriteHandle, $line);
        $iLineCount++;

        if($iLineCount >= $iMaxLines && preg_match('/\);\s*$/i',$line) != false) {
            echo 'Closing ' . $sDirectory . '\\' . $aPathInfo['filename'] . '\\' . $aPathInfo['filename']  . '_' . $i . '.' . $aPathInfo['extension'] . PHP_EOL;
            echo $iLineCount . ' lines written.' . PHP_EOL;

            $i++;
            $iLineCount = 0;

            fclose($oWriteHandle);

            echo 'Starting to write ' . $sDirectory . '\\' . $aPathInfo['filename'] . '\\' . $aPathInfo['filename']  . '_' . $i . '.' . $aPathInfo['extension'] . PHP_EOL;
            $oWriteHandle = fopen($sDirectory . '\\' . $aPathInfo['filename'] . '\\' . $aPathInfo['filename']  . '_' . $i . '.' . $aPathInfo['extension'], 'w');
            
            fwrite($oWriteHandle, $sHeader);
        }
    }

    if($oWriteHandle) {
        echo 'Closing ' . $sDirectory . '\\' . $aPathInfo['filename'] . '\\' . $aPathInfo['filename']  . '_' . $i . '.' . $aPathInfo['extension'] . PHP_EOL;
        echo $iLineCount . ' lines written.' . PHP_EOL;
        fclose($oWriteHandle);
    }
    fclose($oReadHandle);
} else {
    die('error opening the file.');
}

exit;
?>